## Please follow below instructions to push your local docker image to Gcloud Container Service & start a VM from that image.

### I am asuming you guys have basic Docker & Gcloud knowledge. 

Scenario 1. Login to your Gcloud account & login to one of your test vm's

Scenario 2. Login to your test vm on any other platform.

Install gcloud sdk on your linux vm & login to your gcloud account. If your vm is at Gcloud then just login to gcloud since gcloud will already be installed. Its Google home product.

link:- https://cloud.google.com/sdk/docs/#linux

Once you have installed gcloud sdk, follow below commands. I am assuming you guys are using CentOS7 VM. 

[root@iamlinops ~]# gcloud auth login 

[root@iamlinops ~]# yum install docker -y

[root@iamlinops ~]# systemctl start docker

[root@iamlinops ~]# systemctl stop firewalld

[root@ansible ~]# gcloud alpha auth configure-docker

[root@ansible ~]# gsutil ls

[root@ansible ~]# gsutil iam ch allUsers:objectViewer gs://artifacts.**your-project-id**.appspot.com/

[root@iamlinops ~]# docker pull **your-image**

[root@iamlinops ~]# docker tag **image-name** gcr.io/**project-id**/your-image:tag

[root@ansible ~]# docker push gcr.io/**project-id**/your-image:tag

Go to GCP ===> Container Service tab & check the image. Now you can launch a VM with this image from Container Service tab or from Compute Engine. 




